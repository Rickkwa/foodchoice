// app.js Node.js server

"use strict;"   // flag JS errors 

/* Module dependencies:
 *
 * require() loads a nodejs "module" - basically a file.  Anything
 * exported from that file (with "exports") can now be dotted off
 * the value returned by require(), in this case e.g. foodchoice.api
 * The convention is use the same name for variable and module.
 */
var https = require('https'),
	// NOTE, use the version of "express" linked to the assignment handout
	//express = require("./node_modules/express"), // Express Web framework   ... ADD CODE
	express = require("express"), // Express Web framework   ... ADD CODE
	fs = require("fs"),
	// config is just an object, that defines attributes such as "port"
	config = require("./config.js"),  // app's local config - port#, etc
	metrics = require("./metrics.js"),
	foodchoice = require('./routes/foodchoice.js');  // route handlers   ... ADD CODE

var app = express();  // Create Express app server
let prom = new metrics.Metrics();

// Configure app server
app.configure(function() {
	// use PORT environment variable, or local config file value
	app.set('port', process.env.PORT || config.port);

	// change param value to control level of logging  ... ADD CODE
	app.use(express.logger(config.env));  // 'default', 'short', 'tiny', 'dev'

	// use compression (gzip) to reduce size of HTTP responses
	app.use(express.compress());

	// parses HTTP request-body and populates req.body
	app.use(express.bodyParser({
		uploadDir: __dirname + '/public/img/uploads',
		keepExtensions: true
	}));
	
	// session config
    app.use(express.cookieParser());           // populates req.signedCookies
    app.use(express.session({key: config.sessionKey,
		secret: config.sessionSecret,
		cookie: {maxAge:config.sessionTimeout} }));
	
	// Create the uploads directory if it doesn't exist
	if (!fs.existsSync(__dirname + '/public/img/uploads')) {
		fs.mkdirSync(__dirname + '/public/img/uploads', 0755);   
	}

	// Perform route lookup based on URL and HTTP method,
	// Put app.router before express.static so that any explicit
	// app.get/post/put/delete request is called before static
	app.use(app.router);

	// location of app's static content ... may need to ADD CODE
	app.use(express.static(__dirname + "/public"));

	// return error details to client - use only during development
	app.use(express.errorHandler({ dumpExceptions:true, showStack:true }));
});

// App routes (API) - route-handlers implemented in routes/foodchoice.js

// Heartbeat test of server API
app.get('/api', foodchoice.api);
app.get('/healthcheck', foodchoice.healthcheck);
app.get('/metrics', foodchoice.metrics);

// Retrieve a single dish by its id attribute
app.get('/dishes/:id', wrapHandler(foodchoice.getDish));
app.get('/dishes', wrapHandler(foodchoice.getDishes));
app.post('/dishes', wrapHandler(foodchoice.addDish));
app.put('/dishes/:id', wrapHandler(foodchoice.editDish));
app.delete('/dishes/:id', wrapHandler(foodchoice.deleteDish));

app.post('/auth', wrapHandler(foodchoice.signup));
app.get('/auth', wrapHandler(foodchoice.isAuth));
app.put('/auth', wrapHandler(foodchoice.auth));

// Upload an image file and perform image processing on it
app.post('/dishes/image', wrapHandler(foodchoice.uploadImage));

// Catch invalid urls and respond with 404
app.use(function(req, res) {
	prom.httpRequestTotalCounter.inc({ handler: req._parsedUrl.pathname, method: req.method, statuscode: 404 });
	res.send(404, "<h1>404 - Sorry, the requested page does not exist.</h1>Click <a href='/index.html'>here</a> to go back to the app");
});


//https.createServer({key: fs.readFileSync(config.key), cert: fs.readFileSync(config.cert)}, app).listen(app.get('port'), function () {
	//    console.log("Express HTTPS server listening on port %d in %s mode",
	//			app.get('port'), config.env );
	//});

app.listen(app.get('port'), () => console.log('Example app listening on port ' + app.get('port')))

function wrapHandler(handler) {
	return (req, res) => {
		const httpMetricTimer = prom.httpRequestDurationSummary.startTimer({ handler: req.route.path, method: req.method });
		handler(req, res);
		prom.httpRequestTotalCounter.inc({ handler: req.route.path, method: req.method, statuscode: res.statusCode });
		httpMetricTimer({ statuscode: res.statusCode });
		console.log("Finish handler ", req.route.path);
	};
}
