module.exports = {
	"port": 88,
	"env": "dev",
	"db": "mongodb://foodchoice:hunter2@localhost/foodchoice",
	"sessionKey": "sess.foodchoice",
	"sessionTimeout": 1000 * 60 * 2, // 10 min
	"sessionSecret": 'hunter2',
	"key": "ssl/key.pem",
	"cert": "ssl/cert.pem"
}
