echo 'Creating application user and db'

mongo foodchoice \
        --host localhost \
        --port 27017 \
        -u $MONGO_INITDB_ROOT_USERNAME \
        -p $MONGO_INITDB_ROOT_PASSWORD \
        --authenticationDatabase admin \
        --eval "db.createUser({user: 'foodchoice', pwd: 'hunter2', roles:[{role:'readWrite', db: 'foodchoice'}]});"
