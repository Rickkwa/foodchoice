module.exports.Metrics = class Metrics {
    constructor() {
        if (!Metrics.instance) {
            this.client = require('prom-client');

            this.register = new this.client.Registry();
            this.client.collectDefaultMetrics({ register: this.register });
            this.registerMetrics();

            Metrics.instance = this;
        }
        return Metrics.instance;
    }

    registerMetrics() {
        // Initiate metrics
        this._summaries = {
            'http_request_duration_seconds': new this.client.Summary({
                name: 'http_request_duration_seconds',
                help: 'API http request duration in seconds',
                labelNames: ['handler', 'method', 'statuscode'],
                percentiles: [0.01, 0.1, 0.25, 0.5, 0.75, 0.9, 0.99]
            }),
        };
        this._counters = {
            'http_request_total': new this.client.Counter({
                name: 'http_request_total',
                help: 'API http request counter',
                labelNames: ['handler', 'method', 'statuscode']
            }),
        };
        this._gauges = {
        };
        this._histograms = {
        };

        // TODO: some mongo metrics

        for (let metricCollection of [this._summaries, this._counters, this._gauges, this._histograms]) {
            for (let registerKey in metricCollection) {
                this.register.registerMetric(metricCollection[registerKey]);
            }
        }
    }

    get httpRequestDurationSummary() {
        return this._summaries['http_request_duration_seconds'];
    }

    get httpRequestTotalCounter() {
        return this._counters['http_request_total'];
    }
}
