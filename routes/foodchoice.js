"use strict";

// Implement the eatz API:
var fs = require('fs'),
    // GraphicsMagick (gm) for Node is used to resize user-supplied images
    gm = require('gm').subClass({imageMagick: true}),
	config = require(__dirname + '/../config.js'),  // port#, other params
	metrics = require(__dirname + '/../metrics.js'),
	express = require("express"),
	bcrypt = require("bcrypt");


var mongoose = require('mongoose'); // MongoDB integration
// Connect to database, using credentials specified in your config module
mongoose.connect(config.db);
// Schemas
var DishSchema = new mongoose.Schema({
    name: { type: String, required: true },
    venue: { type: String, required: true },
    info: { type: [String], required: false },
    numbr: { type: String, required: true },
    street: { type: String, required: true },
    city: { type: String, required: true },
    province: { type: String, required: true },
    url: { type: String, required: false },
    image: { type: String, required: true },
	lat: { type: Number, required: false },
	lon: { type: Number, required: false }
});

var UserSchema = new mongoose.Schema({
	username: { type: String, required: true, unique: true },
	email: { type: String, required: true},
	password: { type: String, required: true},
});

// each name:venue pair must be unique; duplicates are dropped
DishSchema.index({name: 1, venue: 1}, {unique: true});

// Models
var DishModel = mongoose.model('Dish', DishSchema);
var UserModel = mongoose.model("User", UserSchema);

// Prometheus
const prom = new metrics.Metrics();


// heartbeat response for server API
exports.api = function(req, res){
	res.send(200, '<h3>FoodChoice API is running!</h3>');
};

exports.healthcheck = function(req, res){
	res.send(200, JSON.stringify({
		"api": true,
		"database": mongoose.connection.readyState == 1
	}));
};

exports.metrics = function(req, res){
	res.set({
		'Content-Type': prom.register.contentType,
		'Cache-Control': 'no-cache, no-store, must-revalidate',
		'Pragma': 'no-cache',
		'Expires': '0',
	})
	res.end(prom.register.metrics());
};

// retrieve an individual dish model, using it's id as a DB key
exports.getDish = function(req, res){
	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}
	DishModel.findById(req.params.id, function(err, dish) {
		if (err) {
			res.send(500, "Sorry, unable to retrieve dish at this time (" 
			+err.message+ ")" );
		} else if (!dish) {
			res.send(404, "Sorry, that dish doesn't exist; try reselecting from browse view");
		} else {
			res.send(200, dish);
		}
	});
};

exports.getDishes = function(req, res) {
	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}
	DishModel.find({}, function (err, result) {
		if (!err) {
			res.send(200, result);
		}
		else {
			res.send(404, "Sorry, cannot find any dishes");
		}
	});
}

exports.addDish = function(req, res) {
	// Populate new dish with the given fields
	var d = new DishModel(req.body);

	// Must be logged in
	if (!req.session || !req.session.auth) {
		res.send(403, "You must be logged in to add a dish");
		return;
	}

	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}

	// Save the dish to db
	d.save(function (err, result) {
		if (err) {
			res.send(500, "Could not save because there is already another dish with the same name at the same venue");
		}
		else {
			res.send(200, result);
		}
	});
}

exports.editDish = function(req, res) {
	// Must be logged in
	if (!req.session || !req.session.auth) {
		res.send(403, "You must be logged in to edit a dish");
		return;
	}

	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}

	DishModel.findById(req.params.id, function(err, dish) {
		if (err) {
			res.send(500, "Sorry, unable to retrieve dish at this time (" 
			+err.message+ ")" );
		} else if (!dish) {
			res.send(404, "Sorry, that dish doesn't exist; try reselecting from browse view");
		} else {
			dish.name = req.body.name;
			dish.venue = req.body.venue;
			dish.info = req.body.info;
			dish.numbr = req.body.numbr;
			dish.street = req.body.street;
			dish.city = req.body.city;
			dish.province = req.body.province;
			dish.url = req.body.url;
			dish.image = req.body.image;
			dish.lat = req.body.lat;
			dish.lon = req.body.lon;
			dish.save(function (err, result) {
				if (err) {
					res.send(500, "Could not save because there is already another dish with the same name at the same venue");
				}
				else {
					res.send(200, result);
				}
			});
		}
	});
}

exports.deleteDish = function(req, res) {
	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}
	DishModel.findById(req.params.id, function(err, dish) {
		if (err) {
			res.send(500, "Sorry, unable to retrieve dish at this time (" 
			+err.message+ ")" );
		} else if (!dish) {
			res.send(404, "Sorry, that dish doesn't exist; try reselecting from browse view");
		} else { // found the target dish
			dish.remove(function (err2, dish2) {
				if (err)
					res.send(500, "Could not delete entry");
				else {
					res.send(200, null);
					// TODO: Delete the associated images?
				}
			});
		}
	});
}


exports.uploadImage = function (req, res) {
    var filePath = req.files.image.path, tmpFile, imageURL, writeStream;

	// Repeat this until a unique filename is created
	do {
        tmpFile = generateRandomString(16);
    	imageURL = 'img/uploads/' + tmpFile;
        writeStream = __dirname + '/../public/' + imageURL;
	} while (fs.existsSync(writeStream + "_360.jpg") || fs.existsSync(writeStream + "_240.jpg"));

	// Resize image to 360x270
    gm(filePath).setFormat("jpg").resize(360, 270, "!").write(writeStream + "_360.jpg", function(err) {
		res.set("Content-Type", "application/json");
		if (!err) {
			// Do the second size
			gm(filePath).setFormat("jpg").resize(240, 180, "!").write(writeStream + "_240.jpg", function(err2) {
				if (!err2) {
					res.send(200, {filename: imageURL});
				}
				else {
					res.send(500, "Could not upload the image onto the server. Please try again later.");
				}
				fs.unlink(filePath);
			});
		} else {
			res.send(500, "Could not upload the image onto the server. Please try again later.");
			fs.unlink(filePath);
		}
    });
};


exports.signup = function (req, res) {
	var user = new UserModel(req.body);

	if (mongoose.connection.readyState != 1) {
		res.send(503, "Backend DB unavailable");
		return;
	}

	bcrypt.genSalt(10, function(err, salt) {
        // secure hash of user password with salt value
        bcrypt.hash(user.password, salt, function(err, hash) {
            user.password = hash; // hash encodes both hash-result and salt
            // create DB record for new username
            user.save(function(err, result) {
                if (!err) { // save successful, result contains saved user model
                    req.session.auth = true;
                    req.session.username = result.username;
                    req.session.userid = result.id;
                    res.send({
                        'username': result.username,
                        'userid': result.id
                    });
                } 
                else { // save failed
                    // could alternatively detect duplicate username using find()
					console.log(err);
                    if (err.err.indexOf("E11000") != -1) { // duplicate username error
                        res.send(403, "Sorry, username <b>" + user.username + "</b> is already taken");
                    } 
                    else { // any other DB error
                        res.send(500, "Unable to create account at this time; please try again later " + err.message);
                    }
                }
            });
        });
    });
};

exports.isAuth = function (req, res) {
	if (req.session && req.session.auth == true) {
		res.send({ "userid": req.session.userid, "username": req.session.username });  // return userid and username set to session values
	} 
	else {  // user not authenticated
		res.send({ "username" : null, "userid" : null });  // return userid and username set to null
	};
};

exports.auth = function (req, res) {
	if (req.body.login) {
		var username =  req.body.username;
		var password =  req.body.password;
		if (!username || !password) {  // client should have ensured this, but just in case
			// Missing either username or password field
			res.send(403, "Username and password must not be empty");
			return
		};

		if (mongoose.connection.readyState != 1) {
			res.send(503, "Backend DB unavailable");
			return;
		}

		UserModel.findOne({ "username" : username }, function(err, user){
			if (user) {
				bcrypt.compare(password, user.password , function(err, result) {
					if (result) { // username-password OK
						req.session.auth = true; // user logged in
						req.session.username = user.username;
						req.session.userid = user.id;
						// extend session-life if "remember-me" checked on login form
						if (req.body.rememberme) {
							req.session.cookie.maxAge = config.sessionTimeout * 5;
						} 
						res.send({ "userid": req.session.userid, "username": req.session.username });
					} 
					else { // handle various error conditions
						res.send(403, "The password is incorrect");
					}
				});
			}
			else {
				res.send(403, "The user does not exist");
			}
		});
	}
	else { // logout
		req.session.auth = false;
		req.session.userid = null;
		req.session.username = null;
		res.send({ "userid": null, "username": null });
	}

};

// Generate random alpha numeric string of given size
function generateRandomString(size) {
	var result = "";
	var availStrings = "abcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < size; i++) {
		result = result + availStrings.charAt(Math.floor(Math.random() * availStrings.length));
	}
	return result;
}
