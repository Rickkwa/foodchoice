var foodchoice = foodchoice || {};

foodchoice.Dish = Backbone.Model.extend({
	idAttribute: "_id",
  
	defaults: {
		name: "",
		venue: "",
		info: "",
		numbr: "1265",
		street: "Military Trail",
		city: "Scarborough",
		province: "ON",
		url: "",
		image: "img/placeholder",
		lat: null,
		lon: null
	},
	
	
	initialize: function () {
		// validators object - set of field:function pairs
		this.validators = {};

		// regular expression to test for one or more letter/digit characters (with spaces allowed)
		var letterDigitSpaceRegex = /^[a-zA-Z0-9][a-zA-Z0-9 ]*$/;
		
		// regular expression to test for one or more words (separated by whitespace) where a word is 1 or more letters/digits
		var letterDigitWhiteSpaceRegex = /^[a-zA-Z0-9][a-zA-Z0-9\s]*$/;

		// regular expression to test for a valid url
		// Regex explanation:
			// ^(https?:\/\/)				Start with http:// or https://
			// ([\d\w\.-]+)					Site name/subdomains
			// \.[a-zA-Z]{2,6}				Domain name
			// [a-zA-Z0-9?&#/=%+_\-~,!:]*	Various characters in the url allowed after
		var urlRegex = /^(https?:\/\/)([\d\w\.-]+)\.[a-zA-Z]{2,6}[a-zA-Z0-9?&#/=%+_\-~,!:]*/;
		
		// regular expression to test for 1 or more digits followed by 1 or more letters (eg 1265, 1024B)
		var digitAlphaRegex = /^[0-9]+[A-Za-z]*$/;
		
		// regular expression to test for one or more words (separated by whitespace) where a word is 1 or more letters
		var letterWhiteSpaceRegex = /^[a-zA-Z][a-zA-Z\s]*$/;

		
		this.validators.name = function (value) {
			return (value && letterDigitSpaceRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Name needs to have 1 or more alpha numeric characters"};
		};
		
		this.validators.venue = function (value) {
			return (value && letterDigitSpaceRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Venue needs to 1 or more alpha numeric characters"};
		};
		
		this.validators.info = function (value) {
			return (value && letterDigitWhiteSpaceRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Value must be 1 or more whitespace separated words/digits"};
		};
		
		this.validators.url = function (value) {
			return (value == "" || (value && urlRegex.test(value))) ? {isOK: true} : {isOK: false, errMsg: "Not a valid URL format"};
		};
		
		this.validators.numbr = function (value) {
			return (value && digitAlphaRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Needs to be 1 or more digits followed by optional letters"};
		};
		
		this.validators.street = function (value) {
			return (value && letterWhiteSpaceRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Street must be 1 or more whitespace separated words"};
		};
		
		this.validators.city = function (value) {
			return (value && letterWhiteSpaceRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "City must be 1 or more whitespace separated words"};
		};
	}
});