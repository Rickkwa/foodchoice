var foodchoice = foodchoice || {};

foodchoice.Map = Backbone.Model.extend({
	idAttribute: "_id",
  
	defaults: {
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	},
	
	
	initialize: function (options) {
		var lat = (options && options.lat) ? options.lat : 43.784925;
		var lon = (options && options.lon) ? options.lon : -79.185323;
		this.set({center: new google.maps.LatLng(lat, lon)});
	}
});