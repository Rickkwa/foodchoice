var foodchoice = foodchoice || {};

foodchoice.Dishes = Backbone.Collection.extend({
	model: foodchoice.Dish,
	
	url: "/dishes",

	comparator: function (dish) {
		return dish.get(foodchoice.order).toLowerCase();
	}
});
