var foodchoice = foodchoice || {};

foodchoice.User = Backbone.Model.extend({
	idAttribute: "_id",
  
	defaults: {
		"username": "",
		"password": "",
		"email": ""
	},

	initialize: function () {
		var self = this;
		this.validators = {};

		// regular expression to test for alpha numeric string, allows for underscore, and non-empty
		var userNameRegex = /^[a-zA-Z0-9\_][a-zA-Z0-9\_]*$/;
		
		// regular expression to test for a valid email (simplistic check)
		var emailRegex = /^[a-zA-Z0-9\.\_\-]+@[a-zA-Z0-9\-\_]+\.[a-zA-Z0-9\-\.]+/;

		this.validators.username = function (value) {
			return (value && userNameRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Username must be be non-empty and can only contain underscores or alpha-numeric characters"};
		};
		
		this.validators.email = function (value) {
			return (value && emailRegex.test(value)) ? {isOK: true} : {isOK: false, errMsg: "Not a valid email address"};
		};
		
		this.validators.password = function (value) {
			// It will suffice to check that the password is not empty
			return (value && value.length > 0) ? {isOK: true} : {isOK: false, errMsg: "Password cannot be empty"};
		};
		
		this.validators.password2 = function (value) {
			return (value && value.length > 0 && value == self.get("password")) ? {isOK: true} : {isOK: false, errMsg: "Passwords must match"}
		}
	}
});