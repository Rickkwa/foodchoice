var foodchoice =  foodchoice || {};
var mainTitle = "FoodChoice";

foodchoice.AppRouter = Backbone.Router.extend({
	routes: {
		"": "home",
		"about": "about",
		"dishes": "showDishes",
		"dishes/add": "addDish",
		"dishes/:id": "editDish"
	},

	initialize: function() {
		this.header();

		foodchoice.pubSub = _.extend({}, Backbone.Events);
		this.on("route", function(route, params) {
		    foodchoice.utils.hideNotice();
		});
	},


	home: function() {
		if (!this.homeView) {
			this.homeView = new foodchoice.HomeView();
		};
		$('#content').html(this.homeView.render().el);
		this.homeView.loadCarouselIfNotLoaded();
		
		this.headerView.selectMenuItem(null);
		document.title = mainTitle + " - Home";
	},

	about: function() {
		if (!this.aboutView) {
			this.aboutView = new foodchoice.AboutView();
		};
		$('#content').html(this.aboutView.render().el);
		this.headerView.selectMenuItem("menu-item-about");
		document.title = mainTitle + " - About";
	},

	header: function() {
		if (!this.headerView) {
			this.headerView = new foodchoice.HeaderView();
		};
		$("#header").html(this.headerView.render().el);
	},

	showDishes: function() {
		var self = this;
		this.loadCollection(function (coll, resp) {
			var browseView = new foodchoice.DishesView({collection: coll});
			$("#content").html(browseView.render().el);
			self.headerView.selectMenuItem("menu-item-browse");
			document.title = mainTitle + " - Browse Dishes";
		});
	},

	addDish: function() {
		this.editAddHandler(null, "menu-item-add", mainTitle + " - Add Dish");
	},
	
	editDish: function(id) {
		this.editAddHandler(id, null, mainTitle + " - Edit Dish");
	},
	
	loadCollection: function (successCallback) {
		// Load/Create the collection, then fetch
		if (!this.dishes) {
			this.dishes = new foodchoice.Dishes();
		}
		this.dishes.fetch({success: successCallback});
	},
	
	editAddHandler: function (dishId, menuItemId, title) {
		var self = this;
		this.loadCollection(function (coll, resp) {
			var dish;
			if (dishId == null) {
				dish = new foodchoice.Dish();
				coll.add(dish);
			}
			else
				dish = coll.get(dishId);
				
			var editView = new foodchoice.EditView({model: dish, collection: coll});
			
			$('#content').html(editView.render().el);
			
			var center = editView.mapView.map.getCenter();
			editView.mapView.resize();
			editView.mapView.setCenter(center.k, center.B);
			
			self.headerView.selectMenuItem(menuItemId);
			document.title = title;
		});
	}
});

foodchoice.utils.loadTemplates(['HomeView', 'HeaderView', 'AboutView', 'EditView', 'DishView', 'MapView'], function() {
	app = new foodchoice.AppRouter();
	Backbone.history.start();
});
