var foodchoice =  foodchoice || {};

// note View-name (EditView) matches name of template EditView.html
foodchoice.EditView = Backbone.View.extend({
	initialize: function () {
		this.render();
	},
	
	events: {
		"click #edit-save": "saveDish",
		"click #edit-delete": "deleteDish",
		"change .control-group input": "inputChange",
		"click .upload-image button": "launchFileChooser",
		"dragover .upload-image img": function(e) { e.preventDefault() },
		"drop .upload-image img": "dropPicture",
		"change .upload-image input[type='file']": "chosePicture",
		"click #use-location-btn": "useCurrentLocation"
	},

	render: function () {
		this.$el.html(this.template(this.model.toJSON()));  // create DOM content for EditView
		this.changedImage = false;
		this.changedImageFile = null;
		
		// Do Google map stuff
		var map = (this.model.get("lat") && this.model.get("lon")) ? new foodchoice.Map({lat: this.model.get("lat"), lon: this.model.get("lon")}) : new foodchoice.Map();
		this.mapView = new foodchoice.MapView({model: map});
		var center = this.mapView.map.getCenter();
		$("#map-container", this.$el).html(this.mapView.render().el);
		this.mapView.resize();
		this.mapView.setCenter(center.k, center.B);
			
		return this;
	},
	
	inputChange: function(event) {
		// Create an object containing the name-value pair to update
		var newVals = {};
		newVals[event.target.name] = _.escape(event.target.value);
		
		this.model.set(newVals);
		
		// Validate the model at the field that was changed, and set/remove the error message
		var result = foodchoice.utils.validateField(this.model, event.target.name);
		if (result.isOK == false) {
			foodchoice.utils.failValidation(event.target.id, result.errMsg);
		}
		else {
			foodchoice.utils.passValidation(event.target.id);
		}
	},

	
	saveDish: function (e) {
		e.preventDefault();
		var self = this;
		
		// Validate fields
		if (!foodchoice.utils.validateAllAndSetFeedback(this.model, ".edit-form"))
			return;
			
		var saveCall = function () {
			// Update model and save to db, then redirect to the edit page while displaying a success msg
			self.model.save(null, {
				wait: true,
				success: function (m, resp) {
					foodchoice.AppRouter.prototype.navigate("dishes/" + m.get("_id"), {
						trigger: true, replace: true
					});
					self.render();
					foodchoice.utils.showNotice("success", "Successfully saved!");
				},
				error: function (m, resp) {
					foodchoice.utils.showNotice("error", "Error: " + resp.responseText); 
				}
			});
		};
			
		// Must be logged in to save
		$.get("/auth", {}, function (res) {
			if (res.userid) { // the user is indeed logged in
				if (self.changedImage) {
					foodchoice.utils.uploadFile(self.changedImageFile, function(res, errMsg) {
						if (res && res.filename) {
							self.model.set({image: res.filename});
							saveCall();
						}
						else
							foodchoice.utils.showNotice("error", "Error: " + errMsg);
					});
				}
				else {
					saveCall();
				}
			}
			else
				foodchoice.utils.showNotice("warning", "You must be logged in to save the dish");
		}, "JSON");
		
	},
	
	deleteDish: function (e) {
		e.preventDefault();

		// Destroy and redirect page
		this.model.destroy({
			success: function () {
				foodchoice.AppRouter.prototype.navigate("dishes", {trigger: true, replace: true});
			},
			error: function(m, res) {
				foodchoice.utils.showNotice("error", "Error: " + res.responseText); 
			}
		});
	},
	
	launchFileChooser: function (event) {
		// launchFileChooser should be run when the Snap Photo/Choose file button is clicked.
		// For aesthetic purposes, I want the file chooser to look like a normal button, so the file chooser
		// is invisible and will be triggered when the other button is clicked.
		event.preventDefault();
		$(".upload-image input[type='file']").click();
	},
	
	dropPicture: function (event) {
		event.preventDefault();
		event.originalEvent.dataTransfer.dropEffect = "copy";
		this.changeImage(event.originalEvent.dataTransfer.files[0]);
	},
	
	chosePicture: function (event) {
		this.changeImage(event.target.files[0]);
	},
	
	changeImage: function (imgObj) {
		// Declare a new FileReader object
		var reader = new FileReader();
		
		// Callback for FileReader onload-event; triggers when read completes.
		// Sets "src" attribute to FileReader-object's result
		reader.onload = function (e) {
			$('.upload-image img').attr('src', e.target.result);
		};
		
		this.changedImage = true;
		this.changedImageFile = imgObj;
		
		// Ask FileReader object to read the user-supplied image-object
		reader.readAsDataURL(imgObj);
	},
	
	useCurrentLocation: function (event) {
		event.preventDefault();
		var self = this;
		
		// If supports geolocation then use it
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (result) {
				self.model.set({lat: result.coords.latitude, lon: result.coords.longitude});
				self.render();
				
				// Make the call to geocoder.ca
				$.ajax({
					url: "https://geocoder.ca",
					type: "GET",
					data: {latt: result.coords.latitude, longt: result.coords.longitude, geoit: "xml", reverse: "1"},
					success: function(georesult) {
						$("#numbr").val(georesult.getElementsByTagName("stnumber")[0].childNodes[0].nodeValue);
						$("#street").val(georesult.getElementsByTagName("staddress")[0].childNodes[0].nodeValue);
						$("#city").val(georesult.getElementsByTagName("city")[0].childNodes[0].nodeValue);
						$("#province").val(georesult.getElementsByTagName("prov")[0].childNodes[0].nodeValue);
						self.model.set({
							numbr: $("#numbr").val(),
							street: $("#street").val(),
							city: $("#city").val(),
							province: $("#province").val()
						});
					}
				});
			});
		}
		else {
			foodchoice.utils.showNotice("warning", "Warning: Your browser/device does not support geolocation");
		}
	}
});
