var foodchoice =  foodchoice || {};

foodchoice.MapView = Backbone.View.extend({

	initialize: function () {
		this.render();
	},

	render: function () {
		this.$el.html(this.template(this.model.toJSON()));
		this.map = new google.maps.Map($('#map-content', this.$el)[0], this.model.toJSON());
		this.marker = new google.maps.Marker({position: this.model.get("center"), map: this.map});
		
		return this;
	},

    setCenter: function(lat, lon) {
    	var latlng = new google.maps.LatLng(lat, lon);
		this.model.set({center: latlng});
		this.render();
    },

    resize: function() {
    	google.maps.event.trigger(this.map, 'resize');
    }

});
