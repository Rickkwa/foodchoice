var foodchoice =  foodchoice || {};

// note View-name (DishView) matches name of template DishView.html
foodchoice.DishView = Backbone.View.extend({
	initialize: function () {
		this.render();
	},

	render: function () {
		this.$el.html(this.template(this.model.toJSON())); 
		
		return this;
	},
});
