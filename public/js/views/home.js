var foodchoice =  foodchoice || {};
var isCarouselLoaded = false;

// note View-name (HomeView) matches name of template HomeView.html
foodchoice.HomeView = Backbone.View.extend({

	initialize: function () {
		this.render();
	},

	render: function () {
		this.$el.html(this.template());  // create DOM content for HomeView
		return this;	// support chaining
	},

	// Load the bootstrap carousel
	loadCarouselIfNotLoaded: function () {
		if (isCarouselLoaded === false) {
			var $carouselItems = $(".carousel .carousel-inner .item");
			
			// Remove the active class from all the items
			$carouselItems.removeClass("active");
			
			// Start at a random item
			$($carouselItems[Math.floor(Math.random() * $carouselItems.size())]).addClass("active");
			
			$(".carousel").carousel({
				interval: 6000,
				pause: ""
			});
			isCarouselLoaded = true;
		}
	}
});
