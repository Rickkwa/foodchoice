var foodchoice =  foodchoice || {};

// note View-name (HeaderView) matches name of template HeaderView.html
foodchoice.HeaderView = Backbone.View.extend({

	initialize: function () {
		this.loggedIn = false;
		this.render();
	},

	events: {
		"change #menu-item-sort input[type='radio']": "changeSort",
		"change #signup-dropdown .control-group input": "signupInputChange",
		"change #signin-dropdown .control-group input": "signinInputChange",
		"click #signin-btn": "login",
		"click #signup-btn": "signup",
		"click #signout-btn": "logout"
	},

	render: function () {
		this.$el.html(this.template());  // create DOM content for HeaderView
		foodchoice.order = $("#menu-item-sort input[name='sort-by']:checked", this.$el).val();

		// if authorized from a previous session, show the login css
		var self = this;
		$.get("/auth", {}, function (res) {
			if (res.userid)
				self.signinUserNav(res.username);
		}, "JSON");
		
		return this;
	},

	selectMenuItem: function (menuItemId) {
		$(".navbar-inner .nav li").removeClass("active");
		if (menuItemId !== null)
			$("#" + menuItemId).addClass("active");
	},

	changeSort: function (e) {
		foodchoice.order = $("#menu-item-sort input[name='sort-by']:checked", this.$el).val();
		foodchoice.pubSub.trigger('ordering', e);
	},
	
	signupInputChange: function (e) {
		// An input field on the Sign Up form was changed, do validation on it
		
		// Create an object containing the name-value pair to update
		var newVals = {};
		newVals[e.target.name] = _.escape(e.target.value);
		
		if (!this.model)
			this.model = new foodchoice.User();
		
		this.model.set(newVals);
		
		// Validate the model at the field that was changed, and set/remove the error message
		var result = foodchoice.utils.validateField(this.model, e.target.name);
		if (result.isOK == false) {
			foodchoice.utils.failValidation(e.target.id, result.errMsg);
		}
		else {
			foodchoice.utils.passValidation(e.target.id);
		}
	},
	
	checkLoginField: function (id) {
		var msg;
		if (id == "username-signin")
			msg = "Username cannot be empty";
		else if (id == "password-signin")
			msg = "Password cannot be empty";
		else
			return;

		if ($("#" + id).val().length == 0)
			foodchoice.utils.failValidation(id, msg);
		else
			foodchoice.utils.passValidation(id);
		return $("#" + id).val().length > 0;
	},
	
	signinInputChange: function (e) {
		this.checkLoginField(e.target.id);
	},
	
	login: function (e) {
		e.preventDefault();
		
		// Check username and password not empty
		var usernameCheck = this.checkLoginField("username-signin");
		var passwordCheck = this.checkLoginField("password-signin");
		if (!usernameCheck || !passwordCheck)
			return;
			
		var formData = {
			"username": this.$("#username-signin").val(),
			"password": this.$("#password-signin").val(),
			"rememberme": this.$("#remember-me-signin:checked").length > 0 ? true : false,
			"login": true // so server knows whether the request is for login or logout
		};
		this.doSignInOrUp(formData, "PUT");
	},
	
	signup: function (e) {
		e.preventDefault();
		
		// Validate fields
		if (!this.model)
			this.model = new foodchoice.User();
		if (!foodchoice.utils.validateAllAndSetFeedback(this.model, "#signup-dropdown"))
			return;
		
		// Create form data to send to server via ajax
		var formData = {
			"username" : this.$("#username-signup").val(),
			"email" : this.$("#email-signup").val(),
			"password" : this.$("#password-signup").val()
		};
		
		this.doSignInOrUp(formData, "POST");
	},
	
	logout: function () {
		var self = this;
		var formData = { "login": false };
		$.ajax({
			type: "PUT",
			url: "/auth",
			data: JSON.stringify(formData),
			dataType: "JSON",
			contentType: "application/json",
			success: function (res) {
				self.render();
			},
			error: function (res) {
				foodchoice.utils.showNotice("error", "Error: " + res.responseText);
			}
		});
	},
	
	signinUserNav: function (username) {
		// Change the css of the navbar to show the logged in user
		
		$("#signup-dropdown").hide();
		$("#signin-dropdown .dropdown-toggle").html(username);
		
		$(".signin-container").show().html("Welcome " + username + "!");
		$("#signout-btn").show();
		
		// Hide the form fields
		$("#signin-dropdown .control-group").hide();
		$("#signin-btn").hide();
	},
	
	doSignInOrUp: function (formData, method) {
		// Common code between sign in and sign up
		var self = this;
		$.ajax({
			type: method,
			url: "/auth",
			data: JSON.stringify(formData),
			contentType: "application/json",
			dataType: "json", // expect json result from server
			success: function (res) {
				if (res.userid) {
					$('.nav .dropdown').removeClass('open');
					foodchoice.utils.showNotice("success", "Success! Welcome " + res.username);
					self.signinUserNav(res.username);
				}
				else {
					foodchoice.utils.showNotice("error", "Error getting the result back from the server");
				}
			},
			error: function (res) {
				foodchoice.utils.showNotice("error", "Error: " + res.responseText);
			}
		});
	}
});
