var foodchoice =  foodchoice || {};

foodchoice.DishesView = Backbone.View.extend({
	initialize: function () {
		this.render();

		foodchoice.pubSub.on('ordering', this.render, this);
	},
	
	render: function () {
		// Create the <ul> to contain the dishes
		this.$el.html("<ul id='browse-container' class='page-content page-center thumbnails'></ul>");

		$container = $("#browse-container", this.$el);
		
		// If there are no dishes, display a message
		if (this.collection.length == 0)
			$container.append("<li><h2>Looks like there is nothing here.</h2>Add a new dish from the menu bar above.</li>");
		
		// For each dish, create and load the dish view
		var i = 0;
		this.collection.sort();
		this.collection.each(function (m) {
			var dv = new foodchoice.DishView({model: m});

			$container.append("<li class='thumbnail'><a href='#dishes/" + m.get("_id") + "'>" + dv.render().$el.html() + "</a></li>");
			i += 1;
		});
		
		return this;
	}
});