var foodchoice = foodchoice || {};

foodchoice.utils = {

	// Asynchronously load templates located in separate .html files
	loadTemplates: function(views, callback) {

		var deferreds = [];

		$.each(views, function(index, view) {
			if (foodchoice[view]) {
				deferreds.push($.get('tpl/' + view + '.html', function(data) {
					foodchoice[view].prototype.template = _.template(data);
				}));
			} else {
				console.log(view + " not found");
			}
		});

		$.when.apply(null, deferreds).done(callback);
	},
	
	validateField: function (model, attr) {
		// attr should be a string of one of this model's attributes (eg name, venue, info, etc)
		// If there are rules defined for this attribute
		if (attr && model.validators[attr]) {
			return model.validators[attr](model.get(attr));
		}
		// No rules defined, so it's ok
		return {isOK: true};
	},
	
	validateAllFields: function (model, scope) {
		// scope is a selector that contains all the relevant input fields so that we can retrieve the id
		
		var result = {};
		var allPass = true;
		
		for (var attr in model.validators) {
			result[attr] = this.validateField(model, attr);
			if (result[attr].isOK == false)
				allPass = false;
			result[attr].id = null;
			if ($("input[name='" + attr + "']", scope)[0])
				result[attr].id = $("input[name='" + attr + "']", scope)[0].id;
		}
		
		// Return object to be in the form:
		// {
		// 		isAllOk: boolean,
		// 		results: {
		//			name : {isOk: boolean, errMsg: "...", id: fieldId},
		//			venue : {isOk: boolean, errMsg: "...", id: fieldId},
		//			...
		// 		}
		// }
		return {isAllOK: allPass, results: result};
	},
	
	validateAllAndSetFeedback: function (model, scope) {
		var result = foodchoice.utils.validateAllFields(model, scope);
		if (result.isAllOK == false) {
			// Go through and set the error messages
			for (var key in result.results) {
				// Note: Requires that the model attributes have the same name as the html input name attribute
				if (result.results[key] && result.results[key].isOK == false) {
					foodchoice.utils.failValidation(result.results[key].id, result.results[key].errMsg);
				}
				else {
					foodchoice.utils.passValidation(result.results[key].id);
				}
			}
		}
		return result.isAllOK;
	},

	
	passValidation: function (id) {
		$input = $("#" + id);
		$controlGroup = $input.parents(".control-group");
		// Remove the error message
		$helpMsg = $(".help-inline", $controlGroup).text("");
		// Remove the red
		$controlGroup.removeClass("error");
	},

	
	failValidation: function (id, msg) {
		$input = $("#" + id);
		$controlGroup = $input.parents(".control-group");
		// Set the error message
		$helpMsg = $(".help-inline", $controlGroup).text(msg);
		// Set the red
		$controlGroup.addClass("error");
	},
	
	
	
	uploadFile: function (imgFile, callback) {
		var formData = new FormData();
		formData.append("image", imgFile);
		$.ajax({
			type: "POST",
			url: "/dishes/image",
			data: formData,
			processData: false,
			contentType: false,
			success: function (res) {
				callback(res, null);
			},
			error: function (res) {
				callback(null, res.responseText);
			}
		});
	},

	showNotice: function (alertType, msg) {
		// alertType should be "success", "info", "warning", or "error"
		$(".notice-panel").removeClass("alert-success").removeClass("alert-info").removeClass("alert-warning").removeClass("alert-error");
		$(".notice-panel").html(msg);
		$(".notice-panel").addClass("alert-" + alertType).show();
	},

	hideNotice: function () {
		$(".notice-panel").hide();
	}
};